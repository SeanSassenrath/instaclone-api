const mongoose = require('mongoose');
const Schema = mongoose.Schema;
mongoose.Promise = require('bluebird');

const commentSchema = new Schema({
  comment: { type: String, required: true },
  username: { type: String, required: true },
})

module.exports = mongoose.model('Comment', commentSchema);