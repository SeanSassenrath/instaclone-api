const mongoose = require('mongoose');
const Schema = mongoose.Schema;
mongoose.Promise = require('bluebird');

const postSchema = new Schema({
  likes: { type: Number, default: 0 },
  title: { type: String, required: true },
  author: { type: String, required: true },
  description: { type: String },
  imgUrl: { type: String, required: true },
  comments: [{ type: Schema.Types.ObjectId, ref: 'Comment' }],
}, { timestamps: {} })

module.exports = mongoose.model('Post', postSchema);