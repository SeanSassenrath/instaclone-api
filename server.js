var express = require('express');
var app = express();
var morgan = require('morgan');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
const dotenv = require('dotenv');

dotenv.load();

// Database
mongoose.connect(process.env.MONGO_DB);

// Logs
app.use(morgan('dev'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// CORS
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, PUT');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

// Router
const apiRouter = require('./controller');
app.use('/api', apiRouter);

app.listen(process.env.PORT || 9000, console.log('Server running on port 9000'));