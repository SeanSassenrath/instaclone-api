const Post = require('../../models/post');
const mongoose = require('mongoose');

const postsCtrl = {

  getPosts(req, res) {
    Post.find()
      .then((posts) => {
        res.send(posts);
      })
      .catch((err) => {
        res.send(err);
      });
  },

  postPost(req, res) {
    console.log('req', req.body)
    const newPost = new Post(req.body);
    newPost.save((err, post) => {

      console.log('post', post)
      if (err) {
        console.log('err', err)
         res.send(err) 
      } 
      else {
        res.json({ message: 'Post added', post });
      }
    });
  },
};

module.exports = { postsCtrl };