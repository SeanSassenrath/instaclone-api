const express = require('express');
const { postsCtrl } = require('./postsController');

const posts = express.Router();

posts.use((req, res, next) => {
  next();
});

posts.get('/', postsCtrl.getPosts);
posts.post('/', postsCtrl.postPost);

module.exports = posts;